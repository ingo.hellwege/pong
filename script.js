/* class settings */
class Settings{
  /* constructor */
  constructor(){
    this.setupDefaults();
  }


  /* setup defaults */
  setupDefaults(){
    this.runintval=false;      // game runner intervals setup status
    this.cmp=true;             // computer player
    this.tmr=false;            // timer based game
    this.tmrdef=300;           // timer default length (s)
    this.tmrval=0;             // timer value (s)
    this.run=false;            // timer status
    this.winh=0;               // window height
    this.winw=0;               // window width
    this.padh=0;               // paddle height
    this.padw=60;              // paddle width
    this.padaud='';            // paddle audio
    this.ballSpeedDef=8;       // ball speed default
    this.ballSpeedInc=0.5;     // ball speed increase
    this.ballSpeed=0;          // ball speed
    this.ballX=0;              // default
    this.ballY=0;              // default
    this.ballSpeedX=1;         // default
    this.ballSpeedY=1;         // default
    this.padSpeed=15;          // paddle max speed
    this.padLeftSpeed=0;       // left paddle speed
    this.padRightSpeed=0;      // right paddle speed
    this.run=false;            // game status
    this.fuz=30;               // fizzyness value
    this.grd=0;                // bottom margin
    this.passaud='';           // missed audio
    this.wallaud='';           // wall audio
    this.btnaud='';            // button audio    
  }
}

/* class tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* add class to element */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* get inner html for element by id */
  getElementInnerHtml(elmid){
    return document.getElementById(elmid).innerHTML;
  }

  /* set inner html for element by id */
  setElementInnerHtml(elmid,str){
    document.getElementById(elmid).innerHTML=str;
  }

  /* set width/height (a '-' meand do not) for element by id */
  setSizeForElementById(elmid,px,py){
    if(isFinite(px)){document.getElementById(elmid).style.width=parseInt(px)+'px'};
    if(isFinite(px)){document.getElementById(elmid).style.height=parseInt(py)+'px'};
  }

  /* set position (a '-' means do not) for element by id */
  setPositonForElementById(elmid,px,py){
    if(isFinite(px)){document.getElementById(elmid).style.left=parseInt(px)+"px"};
    if(isFinite(py)){document.getElementById(elmid).style.top=parseInt(py)+"px"};
  }

  /* get x position for element by id */
  getXPositionForElementById(elmid){
    var val=parseInt(document.getElementById(elmid).style.left.replace('px',''));
    return isNaN(val)?0:val;
  }

  /* get y position for element by id */
  getYPositionForElementById(elmid){
    var val=parseInt(document.getElementById(elmid).style.top.replace('px',''));
    return isNaN(val)?0:val;
  }

  /* get visibility for element (class with 'show') */
  isElementVisibleById(elmid){
    return this.getClassStringForElementById(elmid).includes('show')==true;
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* get items for selector */
  getItemsForSelector(sel){
    return document.querySelectorAll(sel);
  }

  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

/* class sound */
class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.padaud=new Audio('pad.wav');
    this.padaud.volume=0.5;
    this.passaud=new Audio('pass.wav');
    this.passaud.volume=0.25;
    this.wallaud=new Audio('wall.wav');
    this.wallaud.volume=0.5;
    this.btnaud=new Audio('button.wav');
    this.btnaud.volume=0.5;
  }
}

/* class game */
class Game{
  /* constructor */
  constructor(){
    // objects
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    // listener
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('resize',this);
    addEventListener('mousemove',this);
    addEventListener('touchmove',this);
  }



  /* set ball position with a bit random vertical */
  setBallStartPosition(){
    console.log('set ball start position');
    // start with more or less middle of the screen
    this.SET.ballX=(window.innerWidth/2)-(this.SET.padw/2);
    this.SET.ballY=(window.innerHeight/2)-(this.SET.padw/2);
    // more up(-) or more down(+) a little bit
    if(Math.random()<0.5){
      this.SET.ballY-=Math.random()*(window.innerHeight/4);
    }else{
      this.SET.ballY+=Math.random()*(window.innerHeight/4);
    }
    // set ball position
    this.TLS.setPositonForElementById('ball',this.SET.ballX,this.SET.ballY);
  }

  /* set the ball moving direction x/y */
  setBallStartMovement(){
    console.log('set ball start movement');
    // factor for desktop and mobile
    var fac=1;
    if(window.innerWidth<=960){fac=0.5;}
    // move horizontal on x left(-) or right(+)
    this.SET.ballSpeedX=(Math.random()*fac)+0.2;
    if(Math.random()<0.5){this.SET.ballSpeedX*=-1;}
    // move vertical on y up(-) or down(+)
    this.SET.ballSpeedY=(Math.random()*fac)+0.1;
    if(Math.random()<0.5){this.SET.ballSpeedY*=-1;}
  }

  /* show ball */
  showBall(){
    this.TLS.addClassToElementById('ball','show-elem');
  }

  /* hide ball */
  hideBall(){
    this.TLS.removeClassFromElementById('ball','show-elem');
  }

  /* vertical angle depending on paddle collision position */
  setBallReflectAngle(which){
    // paddle position
    var pos=this.TLS.getYPositionForElementById('pad-'+which);
    // percentage of ball position to paddle, range it -50 to +50, make it -1 to +1
    var nspd=(((this.SET.ballY-pos)*100/this.SET.padh)-50)/50;
    // change vertical ball speed
    this.SET.ballSpeedY+=nspd
  }

  /* move ball */
  moveBall(){
    // add speed with direction
    this.SET.ballX+=(this.SET.ballSpeed*this.SET.ballSpeedX);
    this.SET.ballY+=(this.SET.ballSpeed*this.SET.ballSpeedY);
    // set position
    this.TLS.setPositonForElementById('ball',this.SET.ballX,this.SET.ballY);
  }



  /* move paddles and slow down automaticly */
  movePaddle(){
    // move left panel depending on game mode (1/2 players)
    if(this.SET.cmp==false){this.setPaddlePosition('left');}
    if(this.SET.cmp==true){this.setComputerPaddle();}
    // right one always
    this.setPaddlePosition('right');
  }

  /* set paddle psotion */
  setPaddlePosition(which){
    // params
    var str=which.charAt(0).toUpperCase()+which.substr(1);
    var vstr='this.SET.pad'+str+'Speed';
    var spd=eval(vstr);
    // set with dynamic variable names
    var my=this.TLS.getYPositionForElementById('pad-'+which)+spd;
    if(my<=0){my=0;eval(vstr+'=0');}
    if(my>=this.SET.winh-(this.SET.padh+this.SET.grd)){my=this.SET.winh-(this.SET.padh+this.SET.grd);eval(vstr+'=0');}
    this.TLS.setPositonForElementById('pad-'+which,'-',my);
    if(spd<0){eval(vstr+'+=this.SET.padSpeed');}
    if(spd>0){eval(vstr+'-=this.SET.padSpeed');}
  }

  /* set right paddle color (for two player mode) */
  setRightPaddleColor(){
    this.TLS.removeClassFromElementById('pad-right','paddle-cmp');
    if(this.SET.cmp==true){this.TLS.addClassToElementById('pad-right','paddle-cmp');}
  }

  /* computer paddle movement */
  setComputerPaddle(){
    // wait before ball is in range
    if((this.SET.ballX<=this.SET.winw*0.4&&this.SET.ballSpeedX<0)||(this.SET.ballX<=this.SET.winw*0.1&&this.SET.ballSpeedX>0)){
      // modifier
      var spd=this.SET.padSpeed;
      // paddle position to ball
      var pos=this.TLS.getYPositionForElementById('pad-left');
      // more middle for comparing to ball
      pos+=this.SET.padh/2;
      // set position a bit more this.SET.fuzzy
      if(this.SET.ballX<=this.SET.padw+10){pos+=((Math.random()*this.SET.fuz)-this.SET.fuz);}
      // change paddle speed smoother
      if(pos<this.SET.ballY-this.SET.fuz){this.SET.padLeftSpeed+=spd;}
      if(pos>this.SET.ballY+this.SET.fuz){this.SET.padLeftSpeed-=spd;}
      // set position
      this.setPaddlePosition('left');
    }
  }

  /* set paddle position from event (mouse or touch) */
  setPaddlePositionFromEvent(mx,my){
    // which paddle (left only for twp player mode)
    var which='';
    if(mx<=this.SET.winw/3&&this.SET.cmp==false){which='left';}
    if(mx>=this.SET.winw-(this.SET.winw/3)){which='right';}
    // set paddle to mouse position
    if(which.length>0){
      // more or less middle positioned to mouse location
      my-=this.SET.padh/1.5;
      // stop at borders
      if(my<=0){my=0;this.SET.padLeftSpeed=0;}
      if(my>=this.SET.winh-(this.SET.padh+this.SET.grd)){my=this.SET.winh-(this.SET.padh+this.SET.grd);this.SET.padLeftSpeed=0;}
      // set position
      this.TLS.setPositonForElementById('pad-'+which,'-',my);
    }
  }



  /* did the ball move off screen */
  checkForMissing(){
    // horizontal or vertical
    if(this.SET.ballX<0||this.SET.ballX>this.SET.winw||this.SET.ballY<-50||this.SET.ballY>this.SET.winh+50){
      this.setPointForPlayer();
    }
  }

  /* check for collision with paddle */
  checkForCollision(){
    // ball, wall and paddle objects
    var ballObj=document.getElementById('ball');
    var wallTopObj=document.getElementById('wall-top');
    var wallBottomObj=document.getElementById('wall-bottom');
    var padLeftObj=document.getElementById('pad-left');
    var padRightObj=document.getElementById('pad-right');
    // check if ball hits a paddle
    if(this.doCollide(ballObj,padLeftObj)||this.doCollide(ballObj,padRightObj)){
      this.TLS.playSound(this.SND.padaud);
      this.SET.ballSpeed+=this.SET.ballSpeedInc;
      this.SET.ballSpeedX*=-1;
      // which paddle
      var which='';
      if(this.doCollide(ballObj,padLeftObj)){which='left';}
      if(this.doCollide(ballObj,padRightObj)){which='right';}
      this.setBallReflectAngle(which);
    }
    // check if ball hits a wall
    if(this.doCollide(ballObj,wallTopObj)||this.doCollide(ballObj,wallBottomObj)){
      this.TLS.playSound(this.SND.wallaud);
      this.SET.ballSpeedY*=-1;
    }
  }

  /* do two object collide */
  doCollide(obj1,obj2) {
    // object bounderies
    var one = obj1.getBoundingClientRect();
    var two = obj2.getBoundingClientRect();
    // check collision
    if(  one.left < two.left   + two.width 
      && one.left + one.width  > two.left 
      && one.top  < two.top    + two.height 
      && one.top  + one.height > two.top) {
        return true;
    }
    return false;
  }



  /* set point for player */
  setPointForPlayer(){
    console.log('set point for player');
    // flag default
    var pass=false;
    // if vertical no player will get a point
    if(this.SET.ballY<-50||this.SET.ballY>this.SET.winh+50){
      pass=true;
    }
    // if horizontal which player gets a point
    if(pass==false&&this.TLS.isElementVisibleById('ball')==true){
      var which='';
      if(this.SET.ballX<0){which='right';}
      if(this.SET.ballX>this.SET.winw){which='left';}
      if(which.length>0){
        this.addPoint(which);
        pass=true;
      }
    }
    // play sound and stop game
    if(pass==true){
      this.SND.passaud;
      this.stopGame();
    }
  }

  /* reset points */
  resetPoints(){
    console.log('reset points');
    this.TLS.setElementInnerHtml('points-left','0');
    this.TLS.setElementInnerHtml('points-right','0');
  }

  /* add point for player */
  addPoint(which){
    console.log('add point for: '+which);
    // adding point for player
    this.TLS.setElementInnerHtml('points-'+which,parseInt(this.TLS.getElementInnerHtml('points-'+which))+1);
  }



  /* set timer value */
  setTimerValue(){
    var m=Math.floor(this.SET.tmrval/60);
    var s=Math.floor(this.SET.tmrval-(m*60));
    var z=(s<10)?'0':'';
    var str=m+':'+z+s;
    this.TLS.setElementInnerHtml('timer',str);
  }

  /* show/hide timer */
  showTimer(){
    this.TLS.removeClassFromElementById('timer','show-elem');
    if(this.SET.tmr==true){this.TLS.addClassToElementById('timer','show-elem');}
  }

  /* run timer */
  runTimer(){
    if(this.SET.tmrrun==true){
      this.SET.tmrval--;
      if(this.SET.tmrval<=0){
        this.stopGame();
        this.SET.tmrrun=false;
      }
    }
    this.setTimerValue();
  }



  /* start game */
  startGame(){
    console.log('start game');
    if((this.SET.tmr==false&&this.SET.run==false)||(this.SET.tmr==true&&this.SET.tmrval>0&&this.SET.run==false)){
      this.SET.ballSpeed=this.SET.ballSpeedDef;
      this.setBallStartPosition();
      this.setBallStartMovement();
      this.showBall();
      this.SET.run=true;
      if(this.SET.tmr==true){this.SET.tmrrun=true;}
    }
  }

  /* this.SET.run game */
  runGame(){
    // if game is running
    if(this.SET.run==true){
      // move and set ball
      this.moveBall();
      // check collisions (ball, paddle, wall)
      this.checkForCollision();
      // horizontal or vertical
      this.checkForMissing();
      // move paddle
      this.movePaddle();
    }
  }

  /* stop game */
  stopGame(){
    console.log('stop game');
    this.SET.run=false;
    this.hideBall();
  }

  /* reset game */
  resetGame(){
    console.log('reset game');
    // setting up
    this.stopGame();
    this.resetPoints();
    this.setRightPaddleColor();
    // ball speed
    this.SET.ballSpeed=this.SET.ballSpeedDef;
    if(window.innerWidth<1024){this.SET.ballSpeed*=0.75;}
    if(window.innerWidth>1600){this.SET.ballSpeed*=1.25;}
    // timer
    if(this.SET.tmr==true){
      this.SET.tmrrun=false;
      this.SET.tmrval=this.SET.tmrdef; 
    }
    this.showTimer();
  }

  /* init game */
  init(){
    console.log('init');
    this.SET.winh=window.innerHeight;
    this.SET.winw=window.innerWidth;
    this.SET.padh=document.getElementById('pad-left').clientHeight+12;
    this.SET.grd=document.getElementById('wall-bottom').offsetHeight*3.25;
    this.resetGame();
    this.TLS.hideCover();
    this.setupRunner();
    // and go
    console.log('ready!');
  }

  /* setup game runner */
  setupRunner(){
    // if runner not done
    if(this.SET.runintval==false){
      // intervall for running the game
      setInterval(()=>{this.runGame();},20);
      // intervall for game timer
      setInterval(()=>{this.runTimer();},1000);
      // setup done
      this.SET.runintval=true;
    }
  }



  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
      case 'mousemove':
        this.mouseMoveEvent(event);
        break;
      case 'touchmove':
        this.touchMoveEvent(event);
        break;
    }
  }

  /* mouse left click event */
  leftClickEvent(event){
    window.dispatchEvent(new KeyboardEvent('keydown',{'code':'KeyS'}));
  }

  /* keypress event */
  keyEvent(event){
    switch(event.code){
      // s(tart)
      case 'KeyS':
        this.TLS.playSound(this.SND.btnaud);
        if(this.SET.run==false){this.startGame();}
        break;
      // r(eset)
      case 'KeyR':
        this.TLS.playSound(this.SND.btnaud);
        this.init();
        break;
      // p(ause)
      case 'KeyP':
        this.TLS.playSound(this.SND.btnaud);
        this.SET.run=!this.SET.run;
      break;
      // left paddle w(up), y(down)
      case 'KeyW':
        this.SET.padLeftSpeed-=this.SET.padSpeed;
        break;
      case 'KeyY':
        this.SET.padLeftSpeed+=this.SET.padSpeed;
        break;
      // right paddle u(up), m(down)
      case 'KeyU':
        this.SET.padRightSpeed-=this.SET.padSpeed;
        break;
      case 'KeyM':
        this.SET.padRightSpeed+=this.SET.padSpeed;
        break;
      // 1(this.SET.cmp=false) or 2(this.SET.cmp=true) player
      case 'Digit1':
        this.TLS.playSound(this.SND.btnaud);
        this.SET.cmp=true;
        this.init();
        break;
      case 'Digit2':
        this.TLS.playSound(this.SND.btnaud);
        this.SET.cmp=false;
        this.init();
        break;
      // t(imer) mode
      case 'KeyT':
        this.TLS.playSound(this.SND.btnaud);
        this.SET.tmr=!this.SET.tmr;
        this.init();
        break;
    }
  }

  /* resize event */
  resizeEvent(event){
    this.stopGame();
    this.init();
  }

  /* mouse move event */
  mouseMoveEvent(event){
    // if game is running
    if(this.SET.run==true){
      // get position
      var my=Math.floor(event.clientY);
      var mx=Math.floor(event.clientX);
      this.setPaddlePositionFromEvent(mx,my);
    }
  }

  /* touch screen event */
  touchMoveEvent(event){
    // if game is running
    if(this.SET.run==true){
      // get position
      var my=Math.floor(event.changedTouches[0].pageY);
      var mx=Math.floor(event.changedTouches[0].pageX);
      this.setPaddlePositionFromEvent(mx,my);
    }
  }
}



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
  }
},250);
